Name: Alex Stubbs
Student No: 20076191

My app is based on a Gym Workout app. 

The functionality of my app includes up to the end of the Models practical. 

Within Contains:

Spash Screen:
I have a basic splash screen that I implemented into my project I learned how to make a splashscreen using this website and edited it to fit my project. http://www.kotlincodes.com/kotlin/android-splash-screen-with-kotlin/

Navigation Button:
As an example for this project that I plan to build on in the next project. I implemented a navigation button when pressed brings you to the GymListActivity screen to create my list. I used this website to learn how to build it and took the time to make it work with my project. https://devofandroid.blogspot.com/2018/03/how-to-move-from-one-activity-to.html

Adding to the list: 
The next page is just a basic blank page with a nav bar and an add button in the right hand corner, thought to us in the practical's I built it using the naming conventions I used in my app.

Adding a Card:
When the user presses the + it opens onto a page where there is 4 fields two are strings the user can input numbers of up to 25 in the Exercise Title and 400 in the Description title. 

Spinner: Within the same page is two drop down menu's which was made using spinner's this took the longest to implement into my project, taking roughly 12 hours of work and research to function in my app.
I origionally used this website: https://devofandroid.blogspot.com/2018/03/how-to-move-from-one-activity-to.html 

But no matter how hard I tried it would never display the result you pressed on the adding the card screen in the ListActivit's always resulting in a blank example meaning it only worked 80% of what I wanted. 

I ended up having to delete after hours of work trying to get it working and instead tired to implement it using this website: https://www.tutorialkart.com/kotlin-android/android-spinner-kotlin-example/ 
Again after 2 hours of trying to do it this way and problem solving with other people in the class we managed to get it working. 

I added in two spinner's into my project, you can now either choose to cancel or confirm the data you have put in. 

Background: What's different with this page is that I added in an image background to the page to give it a different look. I got an image off of google and brought it into the drawable folder under res  I named the photo "background". Than in the .XML file under <RelativeLayout I added the code android:background="@drawable/background".

ListCardView:
Now you should be brought to the page where there should be a card view displaying the data you had just put in. You can now either click on that card view where you can edit the information you put in, or Add another Card view. I had not been able to implement a delete function as everything I tried did not work. From my research I tried to put in a swipe left or right to delete using the "ItemTouchHelper" I found it during research but after multiple attempts failed to get it working so just took out the code. Here is the website I found that showcased the "ItemTouchHelper" https://www.raywenderlich.com/1560485-android-recyclerview-tutorial-with-kotlin#toc-anchor-014


Repository Link: https://bitbucket.org/Stubbsie8/gym/src/master/  You should of recieved an email from me on ddrohan@wit.ie giving you READ ONLY access to view my repository as that is how bitbucket now gives out access links. 
