package ie.alex.gym.main

import android.app.Application
import ie.alex.gym.models.GymMemStore
import ie.alex.gym.models.gymModel
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info

class MainApp : Application(), AnkoLogger {


    val gyms = GymMemStore()

    override fun onCreate() {
        super.onCreate()
        info("Gym started")
        

    }
}
