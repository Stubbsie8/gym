package ie.alex.gym.activities

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ie.alex.gym.R
import ie.alex.gym.models.gymModel
import kotlinx.android.synthetic.main.card_gym.view.description
import kotlinx.android.synthetic.main.card_gym.view.gymTitle
import kotlinx.android.synthetic.main.card_gym.view.Category
import kotlinx.android.synthetic.main.card_gym.view.Category2


interface GymListener {
    fun onGymClick(gym: gymModel)
}




class gymAdapter constructor(
    private var gyms: List<gymModel>,
    private val listener: GymListener
) :  RecyclerView.Adapter<gymAdapter.MainHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainHolder {
        return MainHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.card_gym,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: MainHolder, position: Int) {
        val gym = gyms[holder.adapterPosition]
        holder.bind(gym, listener)
    }

    override fun getItemCount(): Int = gyms.size

    class MainHolder constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(gym: gymModel, listener:GymListener) {
            itemView.gymTitle.text = gym.title
            itemView.description.text = gym.description
            itemView.Category.text = gym.category
            itemView.Category2.text = gym.category2
            itemView.setOnClickListener { listener.onGymClick(gym) }
        }
    }
}