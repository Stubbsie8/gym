package ie.alex.gym.activities

import android.os.Bundle
import android.content.Intent
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import ie.alex.gym.R

class StartUpActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.startupactivity)

        val mStartActBtn = findViewById<Button>(R.id.button)

        mStartActBtn.setOnClickListener {

            startActivity(Intent(this@StartUpActivity, GymListActivity::class.java))
        }

    }
}