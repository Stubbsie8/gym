package ie.alex.gym.activities


import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.*
import ie.alex.gym.R
import ie.alex.gym.main.MainApp
import kotlinx.android.synthetic.main.activity_gym.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import org.jetbrains.anko.toast
import ie.alex.gym.models.gymModel
import kotlinx.android.synthetic.main.activity_gym.description
import kotlinx.android.synthetic.main.activity_gym.gymTitle



class GymActivity : AppCompatActivity(), AnkoLogger {


    var gym = gymModel()
    lateinit var app: MainApp
    var edit = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gym)
        app = application as MainApp
        toolbarAdd.title = title
        setSupportActionBar(toolbarAdd)
        info("Gym Exercise Started")




        var categoryOf = arrayOf<String>("Reps: 1", "Reps: 2", "Reps: 3", "Reps: 4", "Reps: 5", "Reps: 6", "Reps: 7", "Reps: 8", "Reps: 9", "Reps: 10")

        var gymCategory = findViewById(R.id.gymCategory) as Spinner

        var adapter = ArrayAdapter(this,android.R.layout.simple_list_item_1,categoryOf)
        gymCategory.adapter=adapter



        gymCategory.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>, view: View, i: Int, l: Long) {
                Toast.makeText(this@GymActivity, categoryOf[i], Toast.LENGTH_SHORT).show()
            }
            override fun onNothingSelected(adapterView: AdapterView<*>) {
            }
        }






        var categoryOf2 = arrayOf<String>("Sets: 1", "Sets: 2", "Sets: 3", "Sets: 4", "Sets: 5", "Sets: 6", "Sets: 7", "Sets: 8", "Sets: 9", "Sets: 10")

        var gymCategory2 = findViewById(R.id.gymCategory2) as Spinner

        var adapter2 = ArrayAdapter(this,android.R.layout.simple_list_item_1,categoryOf2)
        gymCategory2.adapter=adapter2




        gymCategory2.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>, view: View, i: Int, l: Long) {
                Toast.makeText(this@GymActivity, categoryOf2[i], Toast.LENGTH_SHORT).show()
            }
            override fun onNothingSelected(adapterView: AdapterView<*>) {
            }
        }






        if(intent.hasExtra("gym_edit")){
            edit = true
            gym = intent.extras?.getParcelable<gymModel>("gym_edit")!!
            gymTitle.setText(gym.title)
            description.setText(gym.description)
            gymCategory.setSelection(adapter.getPosition(gym.category))
            gymCategory2.setSelection(adapter.getPosition(gym.category2))
            btnAdd.setText(R.string.save_exercise)
        }








        btnAdd.setOnClickListener() {
            gym.title = gymTitle.text.toString()
            gym.description = description.text.toString()
            gym.category = gymCategory.selectedItem as String
            gym.category2 = gymCategory2.selectedItem as String

            if (gym.title.isEmpty()) {
                toast(R.string.enter_exercise_title)
            } else {
                if (edit) {
                    app.gyms.update(gym.copy())
                } else {
                    app.gyms.create(gym.copy())
                }
            }
            info("add Button Pressed: $gymTitle")
            setResult(AppCompatActivity.RESULT_OK)
            finish()
        }


    }



    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_gym, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.item_cancel -> {
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }


    }






