package ie.alex.gym.activities

import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.gym_list_activity.*
import ie.alex.gym.R
import ie.alex.gym.main.MainApp
import ie.alex.gym.models.gymModel
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.startActivityForResult

class GymListActivity : AppCompatActivity(), GymListener {

    lateinit var app: MainApp

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.gym_list_activity)
        app = application as MainApp

        toolbar.title = title
        setSupportActionBar(toolbar)


        val layoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = gymAdapter(app.gyms.findAll(),this)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.item_add -> startActivityForResult<GymActivity>(0)
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onGymClick(gym: gymModel) {
        startActivityForResult(intentFor<GymActivity>().putExtra("gym_edit",gym), 0)
    }

}

